#include "system_stm32f10x.h"
#include "stm32f10x_rcc.h"
#include "timer_api.h"
#include "control_api.h"
#include "uart_api.h"


void main(void)
{
  SystemInit();
  UART_periph_init();
  GPIO_periph_init();
  TIM_periph_init();
  Init_interrupt_periph();
  while(1)
  {
    
  }
}
