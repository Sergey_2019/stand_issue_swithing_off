#include "stdbool.h"
#include "stm32f10x.h"

#define PORT_LED_TOY            GPIOA
#define PIN_LED_TOY             GPIO_Pin_1
#define PORT_STEND_OUT          GPIOC
#define PIN_LED_ALARM           GPIO_Pin_14
#define PIN_SWITCH_BUTTON       GPIO_Pin_15

extern bool flag_response_toy;

void GPIO_periph_init(void);
void Init_interrupt_periph(void);
void Interrupt_switch(FunctionalState mode);