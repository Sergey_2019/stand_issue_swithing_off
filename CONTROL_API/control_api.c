#include "control_api.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_exti.h"

bool flag_response_toy = false;

void GPIO_periph_init(void)
{
  /* Configure switch button */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
  GPIO_InitTypeDef GPIO_PIN;
  GPIO_PIN.GPIO_Mode = GPIO_Mode_Out_PP;
  GPIO_PIN.GPIO_Pin = PIN_LED_ALARM | PIN_SWITCH_BUTTON;
  GPIO_PIN.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(PORT_STEND_OUT, &GPIO_PIN);
  GPIO_WriteBit(PORT_STEND_OUT, PIN_SWITCH_BUTTON, Bit_SET);
}

void Interrupt_switch(FunctionalState mode)
{
  EXTI_InitTypeDef EXTI_InitStruct;
  EXTI_InitStruct.EXTI_Line = EXTI_Line1;
  /* Enable interrupt */
  EXTI_InitStruct.EXTI_LineCmd = mode;
  /* Interrupt mode */
  EXTI_InitStruct.EXTI_Mode = EXTI_Mode_Interrupt;
  /* Triggers on rising and falling edge */
  EXTI_InitStruct.EXTI_Trigger = EXTI_Trigger_Falling;
  /* Add to EXTI */
  EXTI_Init(&EXTI_InitStruct);
  
  flag_response_toy = false;
}

void Init_interrupt_periph(void)
{
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
  
  GPIO_InitTypeDef GPIO_Struct;
  GPIO_Struct.GPIO_Mode = GPIO_Mode_IPD;
  GPIO_Struct.GPIO_Pin = PIN_LED_TOY;
  GPIO_Struct.GPIO_Speed = GPIO_Speed_10MHz;
  GPIO_Init(PORT_LED_TOY, &GPIO_Struct);
  
  NVIC_InitTypeDef NVIC_LED_TOY;
  NVIC_LED_TOY.NVIC_IRQChannel = EXTI1_IRQn;
  /* Set priority */
  NVIC_LED_TOY.NVIC_IRQChannelPreemptionPriority = 0x00;
  /* Set sub priority */
  NVIC_LED_TOY.NVIC_IRQChannelSubPriority = 0x00;
  /* Enable interrupt */
  NVIC_LED_TOY.NVIC_IRQChannelCmd = ENABLE;
  /* Add to NVIC */
  NVIC_Init(&NVIC_LED_TOY);
  
  GPIO_EXTILineConfig(GPIO_PortSourceGPIOA, GPIO_PinSource1);
  Interrupt_switch(DISABLE);
}

void EXTI1_IRQHandler(void)
{
  if(EXTI_GetFlagStatus(EXTI_Line1))
  {
    flag_response_toy = true;
    /* Clear interrupt flag */
    EXTI_ClearITPendingBit(EXTI_Line1);
  }
}
