#include "stm32f10x_usart.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_gpio.h"
#include "stdarg.h"

void UART_periph_init(void)
{
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
  GPIO_InitTypeDef GPIO_UART;
  GPIO_UART.GPIO_Mode = GPIO_Mode_AF_PP;
  GPIO_UART.GPIO_Pin = GPIO_Pin_9;
  GPIO_UART.GPIO_Speed = GPIO_Speed_10MHz;
  GPIO_Init(GPIOA, &GPIO_UART);
  
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
  USART_InitTypeDef UART_Struct;
  UART_Struct.USART_BaudRate = 115200;
  UART_Struct.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
  UART_Struct.USART_Mode = USART_Mode_Tx;
  UART_Struct.USART_Parity = USART_Parity_No;
  UART_Struct.USART_StopBits = USART_StopBits_1;
  UART_Struct.USART_WordLength = USART_WordLength_8b;
  USART_Init(USART1, &UART_Struct);
  USART_Cmd(USART1, ENABLE);
}

int MyLowLevelPutchar(int x)
{
  while(USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET){}
  USART_SendData(USART1, x);
  return x;
}

//void USARTSend(uint8_t *Buffer)
//{
//  uint8_t cnt = 0;
//  for(uint8_t i=0; Buffer[i] != 0x0A; i++)
//  {
//    cnt++;
//  }
//  cnt++;
//  while(cnt)
//  {
//    USART_SendData(USART1, *Buffer++);
//    while(USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET){}
//    cnt--;
//  }
//}
