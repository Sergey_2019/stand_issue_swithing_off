#include "timer_api.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_tim.h"
#include "stm32f10x_gpio.h"
#include "uart_api.h"
#include "control_api.h"
#include "stdio.h"

uint8_t step = 1;
uint32_t counter_operation = 0;

void delay_ms(uint16_t delay)
{
  TIM_SetAutoreload(TIM2, (delay * 2) - 1);
}

void TIM_periph_init(void)
{
  /* Configure Timer2 */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
  TIM_PrescalerConfig(TIM2, TIMER_DIVIDER, TIM_PSCReloadMode_Immediate);
  TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);
  delay_ms(10);
  NVIC_EnableIRQ(TIM2_IRQn);
  TIM_Cmd(TIM2, ENABLE);
}

void TIM2_IRQHandler(void)
{
  if (TIM_GetITStatus(TIM2, TIM_IT_Update))
  {
    switch(step)
    {
/*********** The loop for indicate alarm **************************************/
    case 0: 
        if(GPIO_ReadOutputDataBit(PORT_STEND_OUT, PIN_LED_ALARM))
          GPIO_ResetBits(PORT_STEND_OUT, PIN_LED_ALARM);
        else
          GPIO_SetBits(PORT_STEND_OUT, PIN_LED_ALARM);
        break;
/******************************************************************************/
/*********************** The toy is turn ON ***********************************/
    case 1:
      counter_operation++;
      printf("Operation_num: %d\r\n", counter_operation);
      GPIO_WriteBit(PORT_STEND_OUT, PIN_SWITCH_BUTTON, Bit_RESET);
      delay_ms(2000);
      step += 1;
      break;
/******************************************************************************/
/************************* Pause **********************************************/
    case 2:
      GPIO_WriteBit(PORT_STEND_OUT, PIN_SWITCH_BUTTON, Bit_SET);
      delay_ms(1500);
      step += 1;
      break;
/******************************************************************************/
/************************* The toy is turn OFF ********************************/
    case 3:
      GPIO_WriteBit(PORT_STEND_OUT, PIN_SWITCH_BUTTON, Bit_RESET);
      Interrupt_switch(ENABLE);
      delay_ms(1200);
      step += 1;
      break;
/******************************************************************************/        
/************************* Pause **********************************************/
    case 4:
      GPIO_WriteBit(PORT_STEND_OUT, PIN_SWITCH_BUTTON, Bit_SET);
      if(flag_response_toy == true)
      {
        delay_ms(2000);
        step = 1;
      }
      else
      {
        printf("Error toy\r\n");
        step = 0;
        delay_ms(200);
      }
      Interrupt_switch(DISABLE);
      break;
/******************************************************************************/
    default:
        break;
    }
    TIM_ClearITPendingBit(TIM2, TIM_IT_Update);
  }
}